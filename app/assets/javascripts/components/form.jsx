class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm1: false,
      showForm2: false
    };
    _.bindAll(this, 'onSelectForm');
  }

  onSelectForm(event) {
    let value = event.target.value;
    if (value === 'one') {
      this.setState({showForm1: true, showForm2: false});
    } else if (value === 'two') {
      this.setState({showForm1: false, showForm2: true});
    } else {
      this.setState({showForm1: false, showForm2: false});
    }
  }

  render() {
    return <div className='form'>
      <h1>Form</h1>
      <select onChange={this.onSelectForm}>
        <option value=''></option>
        <option value='one'>Form1</option>
        <option value='two'>Form2</option>
      </select>
      {(() => {
        if (this.state.showForm1) {
          return <Form1 />;
        }
      })()}
      {(() => {
        if (this.state.showForm2) {
          return <Form2 />;
        }
      })()}
    </div>;
  }
}
